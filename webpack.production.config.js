var path = require('path');
var html = require('html-webpack-plugin');
var node_modules_dir = path.resolve(__dirname, 'node_modules');
// var pathToReact = path.resolve(node_modules_dir, 'react/dist/react.min.js');

module.exports = {
    entry: path.resolve(__dirname, 'client/source/main.jsx'),
    output: {
        path: path.resolve(__dirname, 'client/prod'),
        filename: 'bundle.js'
    },
    plugins: [
        new html()
    ],
    module: {
        loaders: [{
            test: /\.jsx?$/,
            exclude: [node_modules_dir],
            loader: 'babel',
            query: {
              plugins: [
                'transform-decorators-legacy',
                'transform-class-properties',
                'transform-object-assign',
                'transform-object-rest-spread',
              ],
              presets:['react', 'es2015', 'stage-0']
            }
        },{
            test: /\.scss$/,
            loader: 'style!css!sass'
        },{
            test: /\.css$/,
            loader: 'style!css'
        }]
        // ,
        // noParse: [pathToReact]
    }
};
