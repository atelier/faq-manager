/**
 * Entry Model
 */

import {computed, observable} from 'mobx'

import Model from './Model.js'

export default class Entry extends Model {

  @observable question
  @observable answer
  @observable category_id
  @observable status

  constructor(store, json) {
    super(store, json)
  }

  update(json) {
    this.autoSave = false

    this.id           = json.id
    this.question     = json.question
    this.answer       = json.answer
    this.category_id  = json.category_id
    this.status       = json.status

    this.autoSave = true
  }

  @computed get asJSON() {
    return {
      question:     this.question,
      answer:       this.answer,
      category_id:  this.category_id,
      status:       this.status,
    }
  }

  @computed get category() {
    return this.store.repository.categories.byId(this.category_id)
  }
}
