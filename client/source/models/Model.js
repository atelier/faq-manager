/**
 * Base Model
 */
import {autorun, autorunAsync} from 'mobx'

export default class Model {
  id          = null
  store       = null
  saveHandler = null
  autoSave    = false

  /**
   * Creates Model Object
   * @param  {Store}            store Store that Model belongs to
   * @param  {Object|undefined} json  Model attributes
   * @return {Model}
   */
  constructor(store, json) {
    this.store = store

    this.saveHandler = autorun(() => {
      const json = this.asJSON
      
      this.autoSave
        ? this.store.save(this.id, json)
        : null

    })

    json
      ? this.update(json)
      : null
  }

  /**
   * Updates model attributes from server
   * without sending updated data back.
   * Override this metod in your model.
   * @param  {Object} json Attributes
   */
  update(json) {
    this.autoSave = false
    // Update model attributes here
    // ...
    this.autoSave = true

    console.warn('Please override \'update()\' method in your model')
  }

  /**
   * Returns JSON representation of model.
   * Override this getter in your model.
   * @return {Object} [description]
   */
  get asJSON() {
    console.warn('Please override \'asJSON\' getter in your model')
    return {}
  }

  /**
   * Removes autoSave listener
   */
  dispose() {
    this.saveHandler()
  }
}
