/**
 * Category Model
 */

import {computed, observable} from 'mobx'

import Model from './Model.js'

export default class Category extends Model {

  @observable title

  constructor(store, json) {
    super(store, json)
  }

  update(json) {
    this.autoSave = false

    this.id     = json.id
    this.title  = json.title

    this.autoSave = true
  }

  @computed get countEntries() {
    return this.store.repository.entries.all.filter(model =>
      this.id === model.category_id
    ).length
  }

  @computed get asJSON() {
    return {
      title:  this.title
    }
  }
}
