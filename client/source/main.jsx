import React from 'react'
import {render} from 'react-dom'
import {Router, Route, IndexRoute, Redirect, browserHistory} from 'react-router'

import Application      from './components/Application.jsx'
import Front            from './components/Front.jsx'
import FrontRoot        from './components/FrontRoot.jsx'
import FrontEntries     from './components/FrontEntries.jsx'
import Manage           from './components/Manage.jsx'
import ManageRoot       from './components/ManageRoot.jsx'
import ManageEntries    from './components/ManageEntries.jsx'
import EditableEntry    from './components/EditableEntry.jsx'
import EditableCategory from './components/EditableCategory.jsx'

import categoryStore  from './stores/CategoryStore.js'
categoryStore.load()

import entryStore     from './stores/EntryStore.js'
entryStore.load()

import repository     from './stores/Repository.js'

import './styles/main.css'

const mountPoint = document.createElement('div')
document.body.appendChild(mountPoint)

window.db = repository

render(
  <Router history={browserHistory}>
    <Route path="/" component={Application}>
      <IndexRoute component={Front}/>

      <Route path="manage" component={Manage}>
        <Route component={ManageRoot}>
          <IndexRoute component={ManageEntries} />
          <Route component={ManageEntries}>
            <Route path="entry/" component={EditableEntry} />
            <Route path="entry/:entry" component={EditableEntry} />
          </Route>
          <Route path="category/" component={EditableCategory} />
          <Route path="category/:category" component={EditableCategory} />
        </Route>
      </Route>

      <Route path=":category" component={Front}>
        <Route component={FrontRoot}>
          <IndexRoute component={FrontEntries} />
          <Route path="/search/:query" component={FrontEntries} />
        </Route>
      </Route>

    </Route>
  </Router>
, mountPoint)
