/**
 * Entry Store
 */
import {computed} from 'mobx'
import {browserHistory} from 'react-router'

import Store    from './Store.js'
import uiStore  from './UiStore.js'
import Entry    from '../models/Entry.js'

class EntryStore extends Store {
  constructor() {
    super('entries', Entry)
  }

  @computed get countAll() {
    return this.collection.filter(model =>
      (
        uiStore.filterByCategory
          ? model.category_id === uiStore.filterByCategory
          : true
      ) && (
        uiStore.query.length
          ? model.question.toLowerCase().indexOf(uiStore.query.toLowerCase()) >= 0 || model.answer.toLowerCase().indexOf(uiStore.query.toLowerCase()) >= 0
          : true
      )
    ).length
  }

  @computed get countDraft() {
    return this.collection.filter(model =>
      (
        uiStore.filterByCategory
          ? model.category_id === uiStore.filterByCategory
          : true
      ) && (
        model.status === 1
      ) && (
        uiStore.query.length
          ? model.question.toLowerCase().indexOf(uiStore.query.toLowerCase()) >= 0 || model.answer.toLowerCase().indexOf(uiStore.query.toLowerCase()) >= 0
          : true
      )
    ).length
  }

  @computed get countPublished() {
    return this.collection.filter(model =>
      (
        uiStore.filterByCategory
          ? model.category_id === uiStore.filterByCategory
          : true
      ) && (
        model.status === 2
      ) && (
        uiStore.query.length
          ? model.question.toLowerCase().indexOf(uiStore.query.toLowerCase()) >= 0 || model.answer.toLowerCase().indexOf(uiStore.query.toLowerCase()) >= 0
          : true
      )
    ).length
  }

  @computed get filtered() {
    return this.collection.filter(model =>
      (
        uiStore.filterByCategory
          ? model.category_id === uiStore.filterByCategory
          : true
      ) && (
        uiStore.filterByStatus
          ? model.status === uiStore.filterByStatus
          : true
      ) && (
        uiStore.query.length
          ? model.question.toLowerCase().indexOf(uiStore.query.toLowerCase()) >= 0 || model.answer.toLowerCase().indexOf(uiStore.query.toLowerCase()) >= 0
          : true
      )
    )
  }

  @computed get published() {
    return this.collection.filter(model =>
      (
        uiStore.filterByCategory
          ? model.category_id === uiStore.filterByCategory
          : true
      ) && (
        model.status === 2
      ) && (
        uiStore.query.length
          ? model.question.toLowerCase().indexOf(uiStore.query.toLowerCase()) >= 0 || model.answer.toLowerCase().indexOf(uiStore.query.toLowerCase()) >= 0
          : true
      )
    )
  }

  @computed get searched() {
    return this.collection.filter(model =>
      (
        model.status === 2
      ) && (
        uiStore.query.length
          ? model.question.toLowerCase().indexOf(uiStore.query.toLowerCase()) >= 0 || model.answer.toLowerCase().indexOf(uiStore.query.toLowerCase()) >= 0
          : true
      )
    )
  }

  create(json) {
    !json.category_id
      ? json.category_id = 1
      : null

    !json.status
      ? json.status = 1
      : null

    super.create(json, (id) => {
      browserHistory.push('/manage/entry/' + id)
    })
  }
}

const entryStore = new EntryStore()
export default entryStore
