/**
 * UI Store
 */

import {observable} from 'mobx'


class UiStore {

  @observable filterByCategory  = 0
  @observable filterByStatus    = 0
  @observable query             = ''

}

const uiStore = new UiStore()
export default uiStore
