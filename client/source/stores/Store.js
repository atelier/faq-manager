/**
 * Base Store
 */

import axios from 'axios'
import {observable, computed, autorun} from 'mobx'

import repository from './Repository.js'

const request = axios.create({
  baseURL: 'https://calm-headland-61801.herokuapp.com',
  headers: {'Content-Type': 'application/json'}
})

export default class Store {
  repository
  collectionName
  @observable collection = []

  /**
   * Creates Store Object
   * @param  {String} collectionName
   * @param  {Model}  model
   * @param  {Object} repository
   * @return {Store}
   */
  constructor(collectionName, model) {
    this.collectionName = collectionName
    this.model          = model
    this.repository     = repository

    this.repository[this.collectionName] = this

    // this.load()
  }

  load() {
    request.get(this.collectionName)
      .then(response => {
        response.data.map(json => {
          this.update(json)
        }, this)
      })
      .catch(response => console.log(response))
  }

  create(json, callback) {
    request.post(this.collectionName, json)
      .then(response => {
        this.update(response.data)
        callback(response.data.id)
      })
      .catch(response => console.log(response))
  }

  update(json) {
    const model = this.collection.find(model => model.id === json.id)

    model
      ? model.update(json)
      : this.collection.push(new this.model(this, json))
  }

  save(id, json) {
    request.put(this.collectionName + '/' + id, json)
      .then(response => console.log(response))
      .catch(response => console.error(response))
  }

  remove(model) {
    this.collection.splice(this.collection.indexOf(model), 1)
    model.dispose()
  }

  asJSON() {
    return this.collection.map(model => model.asJSON)
  }

  byId(id) {
    return this.collection.find(model => model.id === parseInt(id, 10))
  }

  @computed get all() {
    return this.collection.slice()
  }
}
