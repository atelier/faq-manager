/**
 * Repository
 * An Object that contains references to all Stores
 */
const repository = {
  ui:         {},
  entries:    {},
  categories: {},
}

export default repository
