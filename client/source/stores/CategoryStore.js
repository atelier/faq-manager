/**
 * Category Store
 */
import {computed} from 'mobx'

import Store from './Store.js'
import Category from '../models/Category.js'

class CategoryStore extends Store {
  constructor() {
    super('categories', Category)
  }

  @computed get asSelectableArray() {
    return this.collection.map(model => {
      return {
        id:     model.id,
        title:  model.title,
      }
    })
  }

  @computed get hasPublished() {
    return this.collection.filter(category =>
      category.store.repository.entries.all.filter(entry =>
        entry.status === 2 && entry.category_id === category.id
      ).length > 0
    )
  }
}

const categoryStore = new CategoryStore()
export default categoryStore
