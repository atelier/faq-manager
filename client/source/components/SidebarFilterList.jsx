/**
 * SidebarFilterList Component
 */

import React, {Component, PropTypes} from 'react'
import {observer} from 'mobx-react'
import {Link, browserHistory} from 'react-router'

import entryStore       from '../stores/EntryStore.js'
import uiStore          from '../stores/UiStore.js'

import SidebarListItem  from './SidebarListItem.jsx'
import Button           from './Button.jsx'


export default @observer class SidebarFilterList extends Component {

  static displayName = 'SidebarFilterList'

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)

    this.state = {}
  }

  doFilteringByStatus(e) {
    uiStore.filterByStatus = parseInt(e.currentTarget.getAttribute('data-status'), 10)
  }

  render() {

    return (
      <ul>
        <SidebarListItem
          isActive={!uiStore.filterByStatus}
          label={entryStore.countAll}
        >
          <Button
            onClick={this.doFilteringByStatus}
            data-status="0"
          >
            All statuses
          </Button>
        </SidebarListItem>

        <SidebarListItem
          isActive={uiStore.filterByStatus === 1}
          label={entryStore.countDraft}
        >
          <Button
            onClick={this.doFilteringByStatus}
            data-status="1"
          >
            Draft
          </Button>
        </SidebarListItem>

        <SidebarListItem
          isActive={uiStore.filterByStatus === 2}
          label={entryStore.countPublished}
        >
          <Button
            onClick={this.doFilteringByStatus}
            data-status="2"
          >
            Published
          </Button>
        </SidebarListItem>

        <hr/>

        <SidebarListItem>
          <Link to='/'>View site</Link>
        </SidebarListItem>
      </ul>
    )
  }

}
