/**
 * Form Component
 */

import React, {Component, PropTypes} from 'react'

import Button from './Button.jsx'

export default class Form extends Component {

  static displayName = 'Form'

  static propTypes = {
    namespace:  PropTypes.string,
    onSubmit:   PropTypes.func,
    submitText: PropTypes.string,
  }

  static defaultProps = {
    namespace:  'form-',
    onSubmit:   () => console.log('Form is submitted'),
    submitText: 'Save',
  }

  constructor(props) {
    super(props)

    this.state = {}
    
    this.data = {}
    this.requiredFields = []
    this.isValid = false
  }

  submit = () => this.isValid
    ? this.props.onSubmit(this.data)
    // TODO: Highlight required fields
    : console.log('invalid')

  handleChange = (name, value) => {
    this.data[name] = value

    this.isValid = this.requiredFields.length
      ? this.requiredFields.every(field => !!this.data[field])
      : true
  }

  render() {
    let childrenWithProps = React.Children.map(this.props.children, child => {
      child.props.required
        ? this.requiredFields.push(child.props.name)
        : null

      this.data[child.props.name] = child.props.value

      return React.cloneElement(child, {
        namespace: this.props.namespace,
        onChange: this.handleChange,
      })
    }, this)

    return (
      <form>
        {childrenWithProps}
        <div className="form__footer">
          <Button className='form__submit' onClick={this.submit}>{this.props.submitText}</Button>
        </div>
      </form>
    )
  }

}
