/**
 * EditableCategoryList Component
 * List of Categories with edit link
 */

import React, {Component, PropTypes} from 'react'
import {Link, browserHistory} from 'react-router'
import {observer} from 'mobx-react'

import categoryStore    from '../stores/CategoryStore.js'
import uiStore          from '../stores/UiStore.js'

import SidebarListItem  from './SidebarListItem.jsx'
import Button           from './Button.jsx'


export default @observer class EditableCategoryList extends Component {

  static displayName = 'EditableCategoryList'

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)

    this.state = {}
  }

  doFilteringByCategory(id) {
    uiStore.filterByCategory = parseInt(id, 10)
  }

  render() {
    return (
      <ul>
        <SidebarListItem isActive={!uiStore.filterByCategory}>
          <Button onClick={() => this.doFilteringByCategory(0)}>
            All categories
          </Button>
          <Link to='/manage/category/' className='sidebar__supplement'>
            Add
          </Link>
        </SidebarListItem>
        {
          categoryStore.all.map(category =>
            <SidebarListItem
              isActive={category.id === uiStore.filterByCategory}
              label   ={category.countEntries}
              key     ={`category-list-item-${category.id}`}
            >
              <Button onClick={() => this.doFilteringByCategory(category.id)}>
                {category.title}
              </Button>
              <Link to={`/manage/category/${category.id}`} className='sidebar__supplement'>
                Edit
              </Link>
            </SidebarListItem>
          )
        }
      </ul>
    )
  }

}
