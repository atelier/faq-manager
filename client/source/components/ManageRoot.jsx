/**
 * ManageRoot Component
 */

import React, {Component, PropTypes} from 'react'

import ManageSidebar from './ManageSidebar.jsx'

export default class ManageRoot extends Component {

  static displayName = 'ManageRoot'

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)

    this.state = {}
  }

  render() {

    return (
      <div id="root">
        <ManageSidebar />
        {this.props.children}
      </div>
    )
  }

}
