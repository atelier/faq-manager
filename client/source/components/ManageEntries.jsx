/**
 * ManageEntries Component
 * List of manageable entries
 */

import React, {Component, PropTypes} from 'react'
import {Link} from 'react-router'

import uiStore            from '../stores/UiStore.js'

import EditableEntryList  from './EditableEntryList.jsx'


export default class ManageEntries extends Component {

  static displayName = 'ManageEntries'

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)

    this.state = {}
  }

  doSearching(e) {
    const value = e.currentTarget.value.trim()

    uiStore.query = value
  }

  render() {

    return (
      <main>
        <aside>
          <input
            className='sidebar__search'
            type="search"
            placeholder='Search questions'
            onChange={this.doSearching}
          />
          <Link className='sidebar__add' to="/manage/entry/">Add</Link>
          <hr/>
          <EditableEntryList />
        </aside>

        {this.props.children}
      </main>
    )
  }

}
