/**
 * ViewableEntry Component
 */

import React, {Component, PropTypes} from 'react'

import uiStore from '../stores/UiStore.js'


export default class ViewableEntry extends Component {

  static displayName = 'ViewableEntry'

  static propTypes = {
    entry: PropTypes.object.isRequired
  }

  static defaultProps = {}

  constructor(props) {
    super(props)

    this.state = {}
  }

  componentDidMount() {
    this.highlight()
  }

  componentDidUpdate() {
    this.highlight()
  }

  highlight() {
    this.refs.question.innerHTML = this.mark(uiStore.query, this.props.entry.question)
    this.refs.answer.innerHTML = this.mark(uiStore.query, this.props.entry.answer)
  }

  mark(query, str) {
    const escapedQuery = query.replace(/[-\\^$*+?.()|[\]{}]/g, '\\$&')

    if (escapedQuery.length === 0) {
      return str
    }

    return str.replace(
      RegExp(escapedQuery, 'gi'),
      '<mark>$&</mark>'
    )

  }

  render() {

    return (
      <article>
        <h4 ref='question'></h4>
        <p ref='answer'></p>
      </article>
    )
  }

}
