/**
 * Manage Component
 * Root for everything about Entries and Categories management
 */

import React, {Component, PropTypes} from 'react'

export default class Manage extends Component {

  static displayName = 'Manage'

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)

    this.state = {}
  }

  render() {
    return this.props.children
  }

}
