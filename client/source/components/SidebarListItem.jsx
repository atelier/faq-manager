/**
 * SidebarListItem Component
 */

import React, {Component, PropTypes} from 'react'

export default class SidebarListItem extends Component {

  static displayName = 'SidebarListItem'

  static propTypes = {
    isActive: PropTypes.bool,
    label:    PropTypes.number
  }

  static defaultProps = {}

  constructor(props) {
    super(props)

    this.state = {}
  }

  render() {

    return (
      <li className={'sidebar__item' + (this.props.isActive ? ' sidebar__item--active' : '')}>
        {this.props.children}
        {
          this.props.label >= 0
            ? <div className="sidebar__label">{this.props.label}</div>
            : null
        }
      </li>

    )
  }

}
