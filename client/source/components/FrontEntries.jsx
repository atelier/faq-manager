/**
 * FrontEntries Component
 */

import React, {Component, PropTypes} from 'react'
import {observer} from 'mobx-react'

import categoryStore from '../stores/CategoryStore.js'
import entryStore from '../stores/EntryStore.js'
import uiStore from '../stores/UiStore.js'

import ViewableEntry from './ViewableEntry.jsx'


export default @observer class FrontEntries extends Component {

  static displayName = 'FrontEntries'

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)

    this.state = {}
  }

  render() {

    let collection
    let title

    if (this.props.params.query) {
      collection = entryStore.searched
      title = 'Search results'
    } else {
      collection = entryStore.published
      const category = categoryStore.byId(this.props.params.category)
      title = category
        ? category.title
        : 'All questions'
    }

    return (
      <div className='content'>

        <h3>{title}</h3>
        {
          collection.length
            ? collection.map(entry => <ViewableEntry entry={entry} key={`viewable-entry-${entry.id}`} />)
            : <article>
                <p>No questions found</p>
              </article>
        }
      </div>
    )
  }

}
