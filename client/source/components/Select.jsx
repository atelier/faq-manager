/**
 * Select Component
 * Generic select field
 */

import React, {Component, PropTypes} from 'react'

export default class Select extends Component {

  static displayName = 'Select'

  static propTypes = {
    value:    PropTypes.number.isRequired,
    values:   PropTypes.arrayOf(PropTypes.shape({
      id:       PropTypes.number.isRequired,
      title:    PropTypes.string.isRequired
    })).isRequired,
    onChange: PropTypes.func.isRequired
  }

  static defaultProps = {}

  constructor(props) {
    super(props)

    this.state = {
      id:     props.id,
      name:   props.name,
      value:  props.value,

    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      id:     nextProps.id,
      name:   nextProps.name,
      value:  nextProps.value,
    })
  }

  handleChange = (e) => {
    const value = parseInt(e.currentTarget.value, 10)
    this.props.onChange(value)
    this.setState({value: value})
  }

  render() {
    return (
      <select
        name={this.state.name}
        id={this.state.id}
        value={this.state.value}
        onChange={this.handleChange}
      >
        {
          this.props.values.map(option =>
            <option
              value={option.id}
              key={`${this.state.id}-option-${option.id}`}
            >
              {option.title}
            </option>
          )
        }
      </select>
    )
  }

}
