/**
 * Front Component
 */

import React, {Component, PropTypes} from 'react'
import {browserHistory} from 'react-router'

import uiStore from '../stores/UiStore.js'


export default class Front extends Component {

  static displayName = 'Front'

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)

    this.state = {}
  }

  componentWillMount() {
    this.props.params.category
      ? uiStore.filterByCategory = parseInt(this.props.params.category, 10)
      : browserHistory.push('/1')
  }

  componentWillReceiveProps(nextProps) {
    uiStore.filterByCategory = parseInt(nextProps.params.category, 10) || 1
  }

  render() {
    return this.props.children
  }

}
