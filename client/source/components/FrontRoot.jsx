/**
 * FrontRoot Component
 */

import React, {Component, PropTypes} from 'react'
import {Link, browserHistory} from 'react-router'
import {observer} from 'mobx-react'

import uiStore        from '../stores/UiStore.js'
import categoryStore  from '../stores/CategoryStore.js'


export default @observer class FrontRoot extends Component {

  static displayName = 'FrontRoot'

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)

    this.state = {}
  }

  componentWillReceiveProps(nextProps) {
    !nextProps.params.query
      ? uiStore.query = ''
      : null
  }

  doSearching(e) {
    const value = e.currentTarget.value
    uiStore.query = value
    value
      ? browserHistory.push('/search/' + value)
      : browserHistory.push('/1')

  }

  render() {

    return (
      <div>
        <header>
          <div className='container'>
            <div className='index'>
              <span className="home">FAQ</span>
              <Link to='/manage'>
                <small>Manage entries</small>
              </Link>
            </div>
            <div className='content'>
              <input
                type="search"
                placeholder='Search questions'
                onChange={this.doSearching}
                value={uiStore.query}
              />
            </div>
          </div>
        </header>
        <div className="container">
          <div className="index">
            <h3>Categories</h3>
            <ul>
            {
              categoryStore.hasPublished.map(category =>
                <li className='index__item' key={`category-list-item-${category.id}`}>
                  <Link to={`/${category.id}`} activeStyle={{color:'red'}}>
                    {category.title}
                  </Link>
                </li>
              )
            }
            </ul>
          </div>

          {this.props.children}
        </div>
      </div>
    )
  }

}
