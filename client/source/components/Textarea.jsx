/**
 * Textarea Component
 * Generic textarea field
 */

import React, {Component, PropTypes} from 'react'

export default class Textarea extends Component {

  static displayName = 'Textarea'

  static propTypes = {
    value:    PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired
  }

  static defaultProps = {}

  constructor(props) {
    super(props)

    this.state = {
      id:     props.id,
      name:   props.name,
      value:  props.value,

    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      id:     nextProps.id,
      name:   nextProps.name,
      value:  nextProps.value,

    })
  }

  handleChange = (e) => {
    const value = e.currentTarget.value
    this.props.onChange(value)
    this.setState({value: value})
  }

  render() {
    return (
      <textarea
        {...this.state}
        onChange={this.handleChange}
      />
    )
  }

}
