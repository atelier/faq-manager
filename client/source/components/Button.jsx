/**
 * Button Component
 */

import React, {Component, PropTypes} from 'react'

export default class Button extends Component {

  static displayName = 'Button'

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)

    this.state = {}
  }

  render() {

    return (
      <a href="javascript:;" {...this.props}>
        {this.props.children}
      </a>
    )
  }

}
