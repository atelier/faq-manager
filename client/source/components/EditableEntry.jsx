/**
 * EditableEntry Component
 * Entry management
 */

import React, {Component, PropTypes} from 'react'
import {Link} from 'react-router'
import {observer} from 'mobx-react'

import entryStore     from '../stores/EntryStore.js'
import categoryStore  from '../stores/CategoryStore.js'

import Form           from './Form.jsx'
import FormControl    from './FormControl.jsx'


export default @observer class EditableEntry extends Component {

  static displayName = 'EditableEntry'

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)

    this.state = {
      entry:
        !!this.props.params.entry
          ? entryStore.byId(this.props.params.entry)
          : false
    }

    this.statuses = [{
      id: 1,
      title: 'Draft',
    }, {
      id: 2,
      title: 'Published'
    }]
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      entry:
        !!nextProps.params.entry
          ? entryStore.byId(nextProps.params.entry)
          : false
    })
  }

  updateEntry = (data) => {
    this.state.entry
      ? Object.assign(this.state.entry, data)
      : entryStore.create(data)
  }

  render() {
    const namespace = this.state.entry
      ? `entry-${this.state.entry.id}`
      : 'entry-add'

    const submitText = this.state.entry
      ? 'Save question'
      : 'Create quesion'

    return (
      <section>
        <h2 className="editor__headline">{this.state.entry ? `Q: ${this.state.entry.question}` : 'New question'}</h2>
        <div className="editor__close">
          <Link to="/manage">Close</Link>
        </div>
        <hr/>
        <Form
          onSubmit={this.updateEntry}
          namespace={namespace}
          submitText={submitText}
        >
          <FormControl
            name='question'
            type='textarea'
            label='Question'
            value={this.state.entry.question || ''}
            required
          />
          <FormControl
            name='answer'
            type='textarea'
            label='Answer'
            value={this.state.entry.answer || ''}
          />
          <FormControl
            name='category_id'
            type='select'
            label='Category'
            value={this.state.entry.category_id || 0}
            values={categoryStore.asSelectableArray}
          />
          <FormControl
            name='status'
            type='select'
            label='Status'
            value={this.state.entry.status || 0}
            values={this.statuses}
          />
        </Form>
      </section>
    )
  }

}
