/**
 * FormControl Component
 *
 */

import React, {Component, PropTypes} from 'react'

import Input    from './Input.jsx'
import Textarea from './Textarea.jsx'
import Select   from './Select.jsx'

export default class FormControl extends Component {

  static displayName = 'FormControl'

  static propTypes = {
    name:       PropTypes.string.isRequired,
    type:       PropTypes.string.isRequired,
    label:      PropTypes.string.isRequired,
    value:      PropTypes.any.isRequired,
    values:     PropTypes.arrayOf(PropTypes.shape({
      id:         PropTypes.number.isRequired,
      title:      PropTypes.string.isRequired,
    })),
    required:   PropTypes.bool,
    namespace:  PropTypes.string,
    onChange:   PropTypes.func,
  }

  static defaultProps = {}

  constructor(props) {
    super(props)

    this.component = null

    switch(props.type) {
      case 'textarea':
        this.component = Textarea
        break
      case 'select':
        this.component = Select
        break
      case 'text':
      default:
        this.component = Input
        break
    }

    this.state = {}
  }

  handleChange = (value) => this.props.onChange(this.props.name, value)

  render() {

    return (
      <div className="form__control">
        {
          this.props.label
            ? <label htmlFor={`${this.props.namespace}-${this.props.name}`}>
                {this.props.label}
              </label>
            : null
        }
        {
          React.createElement(this.component, {
            id: `${this.props.namespace}-${this.props.name}`,
            name: this.props.name,
            value: this.props.value,
            values: this.props.values || null,
            onChange: this.handleChange,
          })
        }
      </div>
    )
  }

}
