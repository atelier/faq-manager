/**
 * Application Component
 */

import React, {Component, PropTypes} from 'react'

export default class Application extends Component {

  static displayName = 'Application'

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)

    this.state = {}
  }

  render() {
    return this.props.children
  }

}
