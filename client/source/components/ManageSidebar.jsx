/**
 * ManageSidebar Component
 * Sidebar with filters
 */

import React, {Component, PropTypes} from 'react'
import {Link}     from 'react-router'
import {observer} from 'mobx-react'

import EditableCategoryList from './EditableCategoryList.jsx'
import SidebarFilterList    from './SidebarFilterList.jsx'


export default @observer class ManageSidebar extends Component {

  static displayName = 'ManageSidebar'

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)

    this.state = {}
  }

  render() {

    return (
      <aside>
        <h1>FAQ Manager</h1>
        <hr/>
        <EditableCategoryList />
        <hr/>
        <SidebarFilterList/>
      </aside>
    )
  }

}
