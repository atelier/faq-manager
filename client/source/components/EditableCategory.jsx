/**
 * EditableCategory Component
 * Category management
 */

import React, {Component, PropTypes} from 'react'
import {Link, browserHistory} from 'react-router'
import {observer} from 'mobx-react'

import categoryStore  from '../stores/CategoryStore.js'

import Form           from './Form.jsx'
import FormControl    from './FormControl.jsx'


export default class EditableCategory extends Component {

  static displayName = 'EditableCategory'

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)

    this.state = {
      category:
        !!this.props.params.category
          ? categoryStore.byId(this.props.params.category)
          : false
    }
  }

  updateCategory = (data) => {
    this.state.category
      ? Object.assign(this.state.category, data)
      : categoryStore.create(data)

    browserHistory.push('/manage')
  }

  render() {
    const namespace = this.state.category
      ? `category-${this.state.category.id}`
      : 'category-add'

    const submitText = this.state.category
      ? 'Save category'
      : 'Create category'

    return (
      <section>
        <h2 className="editor__headline">{this.state.category ? `Category: ${this.state.category.title}` : 'New category'}</h2>
        <div className="editor__close">
          <Link to="/manage">Close</Link>
        </div>
        <hr/>
        <Form
          onSubmit={this.updateCategory}
          namespace={namespace}
          submitText={submitText}
        >
          <FormControl
            name='title'
            type='text'
            label='Title'
            value={this.state.category.title || ''}
            required
          />
        </Form>
      </section>
    )
  }

}
