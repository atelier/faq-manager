/**
 * EditableEntryList Component
 */

import React, {Component, PropTypes} from 'react'
import {Link} from 'react-router'
import {observer} from 'mobx-react'

import entryStore from '../stores/EntryStore.js'
import uiStore from '../stores/UiStore.js'

import SidebarListItem from './SidebarListItem.jsx'


export default @observer class EditableEntryList extends Component {

  static displayName = 'EditableEntryList'

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)

    this.state = {}
  }

  render() {

    return (
      <ul>
        {
          entryStore.filtered.length
            ? entryStore.filtered.map(entry =>
                <SidebarListItem key={`entry-list-item-${entry.id}`}>
                  <Link to={`/manage/entry/${entry.id}`}>
                    {entry.question}
                  </Link>
                </SidebarListItem>
              )
            : <SidebarListItem>
                No questions found
              </SidebarListItem>
        }
      </ul>
    )
  }

}
