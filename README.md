FAQ Manager
==

### Requirements
1. Node.js
2. npm

### Installation
`npm install`

### Running
1. `npm start`
1. Open [localhost:8080](http://localhost:8080) to view public FAQ entries
1. Open [localhost:8080/manage](http://localhost:8080/manage) to manage entries
